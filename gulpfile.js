var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minify = require('gulp-minify'),
    jade = require('gulp-jade'),
    sourcemaps = require('gulp-sourcemaps'),
    rename = require('gulp-rename');

gulp.task('sass', function () {
    gulp.src('./src/sass/*.sass')
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/css'));
});


gulp.task('build_css', function () {
    gulp.src('./src/sass/*.sass')
        .pipe(sass({outputStyle: 'compressed'})
            .on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))
        .pipe(rename('main.min.css'))
        .pipe(gulp.dest('dist/css'));
});

gulp.task('jade', function () {
    gulp.src('./src/jade/*.jade')
        .pipe(jade({pretty: true}))
        .pipe(gulp.dest('dist'));
});

gulp.task('watch', function() {
    gulp.watch('./src/sass/**/*.sass', ['sass']);
    gulp.watch('./src/jade/**/*.jade', ['jade']);
});

gulp.task('default', ['sass', 'jade', 'watch']);
